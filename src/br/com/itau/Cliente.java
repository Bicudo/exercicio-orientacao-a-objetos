package br.com.itau;

public class Cliente {
    private String nome;
    private int idade;
    private String cpf;

    public Cliente (String nome, String cpf, int idade){
        this.nome = nome;
        this.cpf = cpf;
        this.idade =idade;
    }

    public String getNome() {

        return nome;
    }

    public void setNome(String nome) {

        this.nome = nome;
    }

    public int getIdade() {

        return idade;
    }

    public void setIdade(int idade) {
        if(idade> 18) {
            this.idade = idade;
        }
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
