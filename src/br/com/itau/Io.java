package br.com.itau;

import sun.security.jgss.wrapper.GSSCredElement;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Io {
    String cpf;
    String nome;
    int idade;

    public static void imprimirMensagemInicial(){
        System.out.println("Bem-vindo ao sistema");
    }

    //DTO receber dados de um console para colocar em um sistema especifico
    public static Map<String, String> solicitarDadosCliente(){
        Scanner scanner= new Scanner(System.in);
        System.out.println("Digite o nome: ");
        String nome = scanner.nextLine();

        System.out.println("Digite o cpf: ");
        String cpf= scanner.nextLine();

        System.out.println("Digite a idade: ");
        //Integer idade = scanner.nextInt();
        String idade = scanner.nextLine();

        Map<String, String> dadosCliente = new HashMap<String, String>();
        dadosCliente.put("nome", nome);
        dadosCliente.put("cpf", cpf);
        dadosCliente.put("idade", idade);

        return dadosCliente;
    }

    //DTO receber dados de um console para colocar em um sistema especifico
    public static Map<String, String> solicitarDadosConta(){
        Scanner scanner= new Scanner(System.in);
        System.out.println("Digite o numero da Conta: ");
        String numeroConta = scanner.nextLine();

        System.out.println("Digite a agencia: ");
        String agencia= scanner.nextLine();

        Map<String, String> dadosConta = new HashMap<String, String>();
        dadosConta.put("numeroConta", numeroConta);
        dadosConta.put("agencia", agencia);

        return dadosConta;
    }

    public static void opcoes(Conta conta) throws IllegalAccessException {
        Scanner scanner = new Scanner (System.in);
        System.out.println("Escolha uma das opcoes: \n1 para saque\n2 para depósito:");
        int opcao= scanner.nextInt();
        if(opcao==1){
            conta.saqueValor(valor());

        }
        else if(opcao==2){
            conta.deposita(valor());
        }

        System.out.println("Saldo Atual:" +conta.getSaldo());
    }

    public static double valor(){
        System.out.println("Digite o valor");
        Scanner scanner = new Scanner(System.in);
        double valor = scanner.nextDouble();
        return valor;
    }
}

