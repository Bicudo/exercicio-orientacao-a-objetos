package br.com.itau;

public class Conta {
    private String numeroConta;
    private String agencia;
    private double saldo;
    private Cliente cliente;

    public Conta(Cliente cliente, String numeroConta, String agencia){
        this.cliente= cliente;
        this.numeroConta = numeroConta;
        this.agencia = agencia;
    }

    public String getNumeroConta() {

        return numeroConta;
    }

    public void setNumeroConta(String numeroConta) {

        this.numeroConta = numeroConta;
    }

    public String getAgencia() {

        return agencia;
    }

    public void setAgencia(String agencia) {

        this.agencia = agencia;
    }

    public double getSaldo() {

        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public void saqueValor (double valor) throws IllegalAccessException {
        double resultado = this.saldo - valor;
        if(resultado <0){
            throw new IllegalAccessException("Valor de saque maior que o saldo disponível, não foi possível efetuar operação");
        }
        else {
            saldo = resultado;
        }
    }

    public void deposita (double valor){

        this.saldo= this.saldo +valor;
    }

    public Cliente getCliente() {

        return cliente;
    }

    public void setCliente(Cliente cliente) {

        this.cliente = cliente;
    }
}
