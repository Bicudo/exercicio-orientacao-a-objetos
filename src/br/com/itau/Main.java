package br.com.itau;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws IllegalAccessException {
        try {
            Map<String, String> dadosCliente = Io.solicitarDadosCliente();
            Cliente cliente = new Cliente(dadosCliente.get("nome"), dadosCliente.get("cpf"), Integer.parseInt(dadosCliente.get("idade")));

            Map<String, String> dadosConta = Io.solicitarDadosConta();
            Conta conta = new Conta(cliente, dadosConta.get("numeroConta"), dadosConta.get("agencia"));
            conta.setSaldo(1000);

            Io.opcoes(conta);
        } catch (IllegalAccessException e){
            e.printStackTrace();
        }

    }
}
